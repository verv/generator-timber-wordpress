var Generator = require('yeoman-generator');
const exec = require('child_process').exec;

module.exports = class extends Generator {

    constructor(args, opts) {
        // Calling the super constructor is important so our generator is correctly set up
        super(args, opts);
    }

    async prompting() {
        this.answers = await this.prompt([
            {
                type: 'input',
                name: 'themeName',
                message: 'Your Theme Name',
                required: true,
            },
            {
                type: 'input',
                name: 'themeURL',
                message: 'Where will the theme be used?'
            },
            {
                type: 'input',
                name: 'themeAuthor',
                message: 'Your Name (will be stored)',
                required: true,
                store: true
            },
            {
                type: 'input',
                name: 'themeAuthorURL',
                message: 'Your Website URL (will be stored)',
                required: true,
                store: true
            },
            {
                type: 'input',
                name: 'themeDescription',
                message: 'A quick description of the theme'
            },
            {
                type: 'confirm',
                name: 'git',
                message: 'Would you like to enable git, and add a basic git ignore file? (Default: Yes)'
            }
        ]);
    }

    writing() {
        // Install Bulma:
        console.log('Setting up NPM');
        this.spawnCommand('npm', ['init', '-y', '--silent']);

        // Setup SCSS:
        this.log('Installing SCSS templates');
        this.fs.copy(
            this.templatePath("scss/**/*.scss"),
            this.destinationPath("scss/"),
        );

        // Setup PHP:
        this.log('Installing PHP files');
        this.fs.copy(
            this.templatePath("php/**/*.php"),
            this.destinationPath("."),
        );

        // Setup Twig Templates:
        this.log('Installing Twig templates');
        this.fs.copy(
            this.templatePath("twig/**/*.twig"),
            this.destinationPath("views/"),
        );

        // Setup our theme info independently:
        this.fs.copyTpl(
            this.templatePath('scss/_themeinfo.scss'),
            this.destinationPath('scss/_themeinfo.scss'),
            {
                themeName: this.answers.themeName,
                themeURL: this.answers.themeURL,
                themeAuthor: this.answers.themeAuthor,
                themeAuthorURL: this.answers.themeAuthorURL,
                themeDescription: this.answers.themeDescription
            }
        );

        // Setup Git Ignore
        if (this.answers.git) {
            this.log('Setting up a .gitignore file...');
            this.fs.copy(
                this.templatePath('gitIgnore.txt'),
                this.destinationPath('.gitignore')
            );
        }
    }

    install() {
        // Install Bulma
        this.log('Installing the latest version of Bulma and Modular Scale');
        this.spawnCommand('npm', ['i', '--save', 'bulma', 'modularscale-sass']);

        // Install Timber:
        this.log('Installing the latest version of Timber for Wordpress');
        this.spawnCommandSync('composer', ['require', 'timber/timber', '--ansi']);
    }

    end() {
        // Setup Git and commit:
        if (this.answers.git) {
            this.log('Committing everything to git');
            this.spawnCommandSync('git', ['init']);
            this.spawnCommandSync('git', ['add', '--all']);
            this.spawnCommandSync('git', ['commit', '-m', '"Initial Commit"']);
        }
    }
};
