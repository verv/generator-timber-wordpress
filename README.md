# generator-timber-wordpress
This [Yeoman](https://yeoman.io/) generator produces a very basic and plain template using Timber and [Bulma](https://bulma.io/). Other support for frameworks may be added in the future (hence why this is not `generator-timber-wordpress-bulma` or something like that).

Since I use [CodeKit](https://codekitapp.com/) during the development process, no build tools are assumed or imposed during setup. You get Timber, and an extremely bare bones Timber theme with some Sass modules available.

## Usage
```
yo timber-wordpress
```
Then, follow the prompts.

## Credits
* David Hoderick's [`generator-shivermetimbers`](https://github.com/davidrhoderick/generator-shivermetimbers) provided inspiration.
